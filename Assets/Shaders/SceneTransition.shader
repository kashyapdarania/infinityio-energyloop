Shader "EnergyLoop/SceneTransition"
{
    Properties
    {
		_Color("Color", Color) = (0,0,0,0) 
		_Center("World Position", Vector) = (0,0,0,0)
		_Radius("Radius", Range(0,15)) = 0
    }
    SubShader
    {
        Blend SrcAlpha OneMinusSrcAlpha
        Tags { "RenderType"="Transparent" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			float4 _Color;
			float4 _Center;
			half _Radius;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
				float4 worldSpacePosition : TEXCOORD1;
            };


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldSpacePosition = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				float3 vertexPos = i.worldSpacePosition;

				float d = distance(_Center, vertexPos);
				if(d < _Radius)
				{
					return fixed4(1,0,0,0);
				}
				else
				{
					return _Color;
				}
            }
            ENDCG
        }
    }
}
