using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnergyLoop.UI
{
    /// <summary>
    /// Responsible for changing size of canvas as per safe area
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class SafeArea : MonoBehaviour
    {
        private void Start()
        {
            var safeArea = Screen.safeArea;
            RectTransform safePanelTransform = GetComponent<RectTransform>();
            // calculate min anchor
            safePanelTransform.anchorMin = CalculateAnchor(safeArea.position);
            // calculate max anchor
            safePanelTransform.anchorMax = CalculateAnchor(safeArea.position + safeArea.size);
        }

        /// <summary>
        /// Calculating anchor as per safe area and return it
        /// </summary>
        /// <param name="_positionOfSafeArea">Calculate area</param>
        /// <returns></returns>
        private Vector2 CalculateAnchor(Vector2 _positionOfSafeArea)
        {
            _positionOfSafeArea.x /= Screen.width;
            _positionOfSafeArea.y /= Screen.height;

            return _positionOfSafeArea;
        }
    }
}
