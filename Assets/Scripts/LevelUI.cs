using UnityEngine;
using UnityEngine.UI;

namespace EnergyLoop.UI
{
    /// <summary>
    /// Responsible for displaying Level UI and handling events
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class LevelUI : MonoBehaviour
    {
        /// <summary>
        /// level bind with this LevelUI
        /// </summary>
        private int level;

        /// <summary>
        /// Responsible for initilizing this levelUI related informations
        /// </summary>
        /// <param name="level"></param>
        public void Initilize(int level, bool isActivated)
        {
            this.level = level;
            GetComponentInChildren<TMPro.TMP_Text>().text = level.ToString();

            // Add click listener
            var btn = GetComponent<Button>();
            btn.onClick.AddListener(OnClick);
            // if not unlocked then it is not clickable
            btn.interactable = isActivated;
        }

        /// <summary>
        /// Called when player clicked on this LevelUI
        /// </summary>
        private void OnClick()
        {
            SessionData.touchPosition = Input.mousePosition;
            OnLevelSelectedEvent.Trigger(level);
        }
    }
}