using System.Collections.Generic;
using UnityEngine;

namespace EnergyLoop
{
    /// <summary>
    /// Holding all levels details
    /// </summary>
    [CreateAssetMenu(fileName = "Levels")]
    public class Levels : ScriptableObject
    {
        public List<LevelDetails> levels;
    }
}