using DG.Tweening;
using EnergyLoop.Managers;
using UnityEngine;

namespace EnergyLoop.Gameplay
{
    /// <summary>
    /// Responsible for node handling
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    public class Node : MonoBehaviour, IDraggable
    {
        /// <summary>
        /// Used to scale down or up when placing on the grid
        /// </summary>
        public const float INITIAL_SCALE_MULTIPLIER = 0.98f;

        /// <summary>
        /// Default camera position, use with camera shake (This is static so only one object will be created)
        /// </summary>
        private static Vector3 cameraPosition = new Vector3(0, 0, -10f);

        /// <summary>
        /// Scale while dragging
        /// </summary>
        private const float SCALE_WHILE_DRAGGIN = 1.1f;
        /// <summary>
        /// Sorting order while this node dragging
        /// </summary>
        private const int SORTING_ORDER_WHILE_DRAGGING = 10;
        /// <summary>
        /// Moving speed for swapping and reseting
        /// </summary>
        private const float MOVING_SPEED = 0.2f;

        /// <summary>
        /// Unique id
        /// </summary>
        public int Id;

        /// <summary>
        /// Id of current node
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// Is current node is static?
        /// </summary>
        public bool Static;

        /// <summary>
        /// position in the grid (x = row, y = column)
        /// </summary>
        public Vector2Int positionInGrid;

        Transform IDraggable.transform { get => transform; }

        private SpriteRenderer spriteRenderer;
        private Canvas canvas;
        /// <summary>
        /// position holder when drag start (needed to reset to its original position if there is no swap possible)
        /// </summary>
        private Vector2 positionWhenDragStart;

        /// <summary>
        /// current node state
        /// </summary>
        private NodeState currentState;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            canvas = GetComponentInChildren<Canvas>();
            // initial state should be idle
            currentState = NodeState.Idle;
        }

        private void Start()
        {
            // setting random color for non-static nodes
            if (Static)
            {
                spriteRenderer.color = Utilities.staticNodeColor;
            }
            else
            {
                // used random with paramerter becuase black is for static nodes
                spriteRenderer.color = Utilities.GetNodeColor();
            }

            transform.localScale *= INITIAL_SCALE_MULTIPLIER;
        }

        /// <summary>
        /// Called from InputManger when drag start
        /// </summary>
        /// <returns></returns>
        public bool OnDragStart()
        {
            // if static, no need to drag this node
            if (Static)
            {
                // return false so InputManager ignore this node for dragging
                return false;
            }

            // if it is already moving, no need to drag this node
            if (currentState == NodeState.Moving)
            {
                // return false so InputManager ignore this node for dragging
                return false;
            }

            // update current state
            currentState = NodeState.Moving;
            positionWhenDragStart = transform.position;

            // update sorting order so that always dragging node render above all other nodes
            SetSortingOrder(SORTING_ORDER_WHILE_DRAGGING);

            // scale up the node so it looks like picked up
            transform.DOKill();
            transform.DOScale(SCALE_WHILE_DRAGGIN, 0.1f);

            SoundManager.Instance.PlaySFX(SFXType.PickNode);

            // returing true so InputManager start dragging of this node
            return true;
        }

        /// <summary>
        /// Called from InputManger while player dragging this node
        /// </summary>
        public void OnDragging()
        {
        }

        /// <summary>
        /// Called from InputManger on drag end
        /// </summary>
        public void OnDragEnd()
        {
        }

        private void SetSortingOrder(int order)
        {
            spriteRenderer.sortingOrder = order;
            canvas.sortingOrder = order;
        }

        /// <summary>
        /// used with Swap to update position in the grid and its actual position in the world
        /// </summary>
        /// <param name="positionInGrid"></param>
        /// <param name="position"></param>
        public void UpdatePosition(Vector2Int positionInGrid, Vector2 position)
        {
            // update current state
            currentState = NodeState.Moving;

            this.positionInGrid = positionInGrid;

            // move toward this position
            MoveToPosition(position);
        }

        /// <summary>
        /// Used to move this node to specific position
        /// </summary>
        /// <param name="position">target position</param>
        private void MoveToPosition(Vector3 position)
        {
            SetSortingOrder(SORTING_ORDER_WHILE_DRAGGING);

            transform.DOMove(position, MOVING_SPEED).onComplete += () =>
            {
                // update current state
                currentState = NodeState.Idle;
                // reset sorting order
                SetSortingOrder(0);

                // scale down
                transform.DOKill();
                transform.DOScale(1 * INITIAL_SCALE_MULTIPLIER, 0.1f);

                SoundManager.Instance.PlaySFX(SFXType.NodeDrop);
            };
        }

        /// <summary>
        /// used with swap for geting initial position of another node
        /// </summary>
        /// <returns></returns>
        public Vector2 GetBeforeDragPosition()
        {
            return positionWhenDragStart;
        }

        /// <summary>
        /// Called from InputManger when there is no swap possible and node must have to reset to its position
        /// </summary>
        public void OnReset()
        {
            // Note currently using Unity's vibrator to vibrate but we can use plugin.
            // so we can vibrate for different feel like warning, error etc.
            Handheld.Vibrate();
            CameraShake();

            MoveToPosition(positionWhenDragStart);
        }

        /// <summary>
        /// Camera shake
        /// </summary>
        private void CameraShake()
        {
            var camera = Camera.main;
            camera.transform.position = cameraPosition;
            // camera shake basically uses sin and cos wave to create shake
            camera.DOShakePosition(0.5f, 1f).onComplete += () =>
            {
                camera.transform.position = cameraPosition;
            };
        }
    }
}