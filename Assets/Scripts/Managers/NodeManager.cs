using DG.Tweening;
using EnergyLoop.Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace EnergyLoop.Managers
{
    public enum NodeState
    {
        Idle,
        Moving
    }

    /// <summary>
    /// Handling creation of all nodes and win condition
    /// </summary>
    public class NodeManager : MonoBehaviour
    {
        private const float NODE_ANIMATION_TIME = 0.2f;
        private const float NODE_ANIMATION_DELAY = 0.09f;

        private GridCreator gridCreator;
        private LevelDetails currentLevel;

        /// <summary>
        /// parent of nodes
        /// </summary>
        [SerializeField]
        private Transform nodeParent = default;

        /// <summary>
        /// Created nodes
        /// </summary>
        public List<Node> nodes;
        public NodeSaveData nodesForSaving;

        private void Awake()
        {
            nodes = new List<Node>();
        }

        /// <summary>
        /// Responsible for creating nodes for current level
        /// </summary>
        /// <param name="gridCreator">grid creator for cell positions</param>
        /// <param name="level">current level details</param>
        public void Create(GridCreator gridCreator, LevelDetails level)
        {
            this.gridCreator = gridCreator;
            this.currentLevel = level;

            // load already saved nodes
            nodesForSaving = SaveManager.Instance.LoadLevel(currentLevel.id);
            // if there is nothing saved for this level, initilize save object
            if (nodesForSaving == null)
            {
                nodesForSaving = new NodeSaveData();
                nodesForSaving.nodes = new List<NodeData>();
            }

            // create static nodes
            var staticNodes = currentLevel.staticNodes;
            foreach (var node in staticNodes)
            {
                CreateNode(node, isStatic: true, node.position);
            }

            // Assign unique id to each node(used to check win condition)
            var dynamicNodes = currentLevel.dynamicNodes;
            AssignUniquIdToNodes(dynamicNodes);

            // if count is greater then we are loading existing level
            // else create new level with randomly placing nodes
            List<Vector2Int> nodePositions = new List<Vector2Int>();
            bool isLoadingSavedLevel;
            if (nodesForSaving.nodes.Count > 0)
            {
                isLoadingSavedLevel = true;
                nodePositions = nodesForSaving.nodes.Select(node => node.position).ToList();
            }
            else
            {
                isLoadingSavedLevel = false;

                // saving the position of every nodes, so in future we can reorder them to create random nodes on grid
                foreach (var node in dynamicNodes)
                {
                    nodePositions.Add(node.position);
                }

                // Rearrange nodes to random positions
                nodePositions = Shuffle(nodePositions);
            }

            // generate nodes at random positions
            CreateDynamicNodes(nodePositions, isLoadingSavedLevel);
        }

        /// <summary>
        /// Creating nodes at random positions
        /// </summary>
        /// <param name="randomPositions">Which positions should be use (length should be same as dynamicNodes)</param>
        private void CreateDynamicNodes(List<Vector2Int> randomPositions, bool isLoadingSavedLevel = false)
        {
            // we can not create nodes if both count is not equal
            if (randomPositions.Count != currentLevel.dynamicNodes.Count)
            {
                return;
            }
            Node createdNode;
            Vector2Int positionInGrid;
            List<NodeData> nodesData = new List<NodeData>(currentLevel.dynamicNodes);
            foreach (var node in nodesData)
            {
                // getting random position
                positionInGrid = randomPositions[node.id];
                createdNode = CreateNode(node, isStatic: false, positionInGrid);
                nodes.Add(createdNode);

                if (!isLoadingSavedLevel)
                {
                    nodesForSaving.nodes.Add(new NodeData() { id = node.id, nodeId = node.nodeId, position = positionInGrid });
                }
            }
        }

        /// <summary>
        /// Create node at specific position
        /// </summary>
        /// <param name="node">node to create</param>
        /// <param name="isStatic">is static or dynamic?</param>
        /// <param name="positionInGrid">position in the grid</param>
        /// <returns></returns>
        private Node CreateNode(NodeData node, bool isStatic, Vector2Int positionInGrid)
        {
            Node createdNode;

            string path = Path.Combine("Nodes", node.nodeId.ToString());
            createdNode = Instantiate(Resources.Load<Node>(path));
            createdNode.transform.SetParent(nodeParent);
            createdNode.transform.localScale = Vector3.one;
            createdNode.transform.position = this.gridCreator.GetCellPosition(positionInGrid.x - 1, positionInGrid.y - 1);
            createdNode.positionInGrid = positionInGrid;
            createdNode.Static = isStatic;
            createdNode.NodeId = node.id;
            createdNode.Id = node.id;

            return createdNode;
        }

        /// <summary>
        /// Assign unique id to each node so we can use to check win condition
        /// </summary>
        /// <param name="dynamicNodes"></param>
        private static void AssignUniquIdToNodes(List<NodeData> dynamicNodes)
        {
            // assign id (unique) to each dynamic nodes
            int index = 0;
            foreach (var node in dynamicNodes)
            {
                node.id = index++;
            }
        }

        /// <summary>
        /// Switch two nodes
        /// </summary>
        /// <param name="dragging">dragging node</param>
        /// <param name="existing">node that already placed on grid</param>
        public void SwitchNode(IDraggable dragging, IDraggable existing)
        {
            Node draggingNode = dragging.transform.GetComponent<Node>();
            Node existingNode = existing.transform.GetComponent<Node>();

            if (draggingNode == null || existingNode == null)
            {
                return;
            }

            // if node that is already on the grid is static, no need to swap
            if (existingNode.Static)
            {
                // reset currant dragging node to its position
                draggingNode.OnReset();
                return;
            }
            else
            {
                // switch the position in the grid and move toward each other position
                Vector2Int draggingNodePositionInGrid = draggingNode.positionInGrid;
                draggingNode.UpdatePosition(existingNode.positionInGrid, existingNode.transform.position);
                existingNode.UpdatePosition(draggingNodePositionInGrid, draggingNode.GetBeforeDragPosition());
            }

            // whenever swap occurs, there is chance that game may completes
            CheckGameCompleteCondition();

            if (SessionData.currentGameState == GameState.Gameplay)
            {
                // save updated nodes
                nodesForSaving.nodes.SingleOrDefault(node => node.id == draggingNode.Id).position = draggingNode.positionInGrid;
                nodesForSaving.nodes.SingleOrDefault(node => node.id == existingNode.Id).position = existingNode.positionInGrid;
            }
        }

        /// <summary>
        /// Save nodes to file
        /// </summary>
        private void SaveNodes()
        {
            SaveManager.Instance.SaveLevel(currentLevel.id, nodesForSaving);
        }

        /// <summary>
        /// Check game is completed or not based on position of all the nodes 
        /// (if all node's position as same as defined in the level, then level completed) 
        /// </summary>
        private void CheckGameCompleteCondition()
        {
            bool isGameCompleted = true;

            var nodesFromLevel = currentLevel.dynamicNodes;

            Node nodeToCheck;
            foreach (var nodeData in nodesFromLevel)
            {
                nodeToCheck = nodes.Single(node => node.positionInGrid.Equals(nodeData.position));
                if (nodeData.nodeId != nodeToCheck.GetComponent<NodeId>().id)
                {
                    isGameCompleted = false;
                }
            }

            if (isGameCompleted)
            {
                // save completed level to file
                if (SaveManager.Instance.GetLastCompletedLevel() < currentLevel.id)
                {
                    SaveManager.Instance.SaveLastCompletedLevel(currentLevel.id);
                }

                GameManager.Instance.ChangeState(GameState.PostGameplay);

                // animate dynamic nodes one by one
                StartCoroutine(AnimateNodes(() =>
                {
                    SoundManager.Instance.PlaySFX(SFXType.LevelComplete);
                    // change state to level complete
                    OnLevelCompletedEvent.Trigger(currentLevel.id);
                }));
            }
            else
            {
                SaveNodes();
            }
        }

        /// <summary>
        /// Animate dynamic nodes one by one
        /// </summary>
        /// <param name="callback">callback to call when animation complete</param>
        /// <returns></returns>
        private IEnumerator AnimateNodes(Action callback)
        {
            yield return new WaitForSeconds(0.5f);
            var dynamicNodes = currentLevel.dynamicNodes;

            Node nodeToAnimate;
            for (int i = 0; i < dynamicNodes.Count; i++)
            {
                nodeToAnimate = nodes.Single(n => n.Id == dynamicNodes[i].id);
                var scaleSequence = DOTween.Sequence();
                scaleSequence.Append(nodeToAnimate.transform.DOScale(1.2f, NODE_ANIMATION_TIME));
                scaleSequence.Append(nodeToAnimate.transform.DOScale(1f * Node.INITIAL_SCALE_MULTIPLIER, NODE_ANIMATION_TIME));
                scaleSequence.Play();

                SoundManager.Instance.PlaySFX(SFXType.ButtonTap);

                yield return new WaitForSeconds(NODE_ANIMATION_DELAY);
            }

            callback?.Invoke();
        }

        /// <summary>
        /// Used to shuffle list (this will 100% random becuase of recursion)
        /// </summary>
        /// <typeparam name="TValue">list to change</typeparam>
        /// <param name="list">list after change</param>
        /// <returns></returns>
        private List<TValue> Shuffle<TValue>(List<TValue> list)
        {
            List<TValue> shuffledList = list.OrderBy(x => UnityEngine.Random.value).ToList();
            if (shuffledList.SequenceEqual(list))
            {
                return Shuffle(list);
            }

            return shuffledList;
        }
    }
}
