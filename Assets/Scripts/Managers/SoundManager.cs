using System.Collections.Generic;
using UnityEngine;

namespace EnergyLoop.Managers
{
    /// <summary>
    /// SFX enum to bind clips
    /// </summary>
    public enum SFXType
    {
        ButtonTap,
        NodeDrop,
        LevelComplete,
        PickNode
    }

    /// <summary>
    /// Responsible for playing sounds
    /// </summary>
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Instance;

        /// <summary>
        /// Audio sources those can be used to play sfxs
        /// </summary>
        [SerializeField]
        private AudioSource[] sfxSources = default;

        /// <summary>
        /// Audio clips (order should be same as SFXType enum)
        /// </summary>
        [SerializeField]
        private List<AudioClip> sfxClips = default;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
                return;
            }
        }

        /// <summary>
        /// Play specific sfx for one time
        /// </summary>
        /// <param name="type">sfx to play</param>
        public void PlaySFX(SFXType type)
        {
            // iterate throught audio sources, play sfx if current audio sources is not busy
            foreach (var source in sfxSources)
            {
                if (!source.isPlaying)
                {
                    source.PlayOneShot(sfxClips[(int)type]);
                    break;
                }
            }
        }
    }

}