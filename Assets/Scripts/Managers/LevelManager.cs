using EnergyLoop.EventsManager;
using EnergyLoop.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EnergyLoop.Managers
{
    /// <summary>
    /// Responsible for handling creation of level ui and events
    /// </summary>
    public class LevelManager : MonoBehaviour, EventListener<OnLevelSelectedEvent>
    {
        public static LevelManager Instance;

        [SerializeField]
        private Button playButton = default;

        /// <summary>
        /// prefab of level selection ui
        /// </summary>
        [SerializeField]
        private LevelUI levelUIPrefab = default;

        /// <summary>
        /// where all level ui object create?
        /// </summary>
        [SerializeField]
        private Transform levelUIParent = default;

        private void Start()
        {
            CreateLevels();
        }

        /// <summary>
        /// Create levels button
        /// </summary>
        private void CreateLevels()
        {
            List<LevelDetails> levels = GameManager.Instance.GetLevels().levels;

            LevelUI levelUI;
            int lastCompletedLevel = SaveManager.Instance.GetLastCompletedLevel();
            SessionData.LastCompletedLevel = lastCompletedLevel;
            int levelId;
            for (int i = 0; i < levels.Count; i++)
            {
                levelId = levels[i].id;
                levelUI = Instantiate(levelUIPrefab, levelUIParent);

                // make it non-interactable if is not unlocked
                levelUI.Initilize(levelId, levelId <= lastCompletedLevel + 1);
            }

            // disable play button if all levels are completed
            playButton.gameObject.SetActive(lastCompletedLevel < levels.Count);
        }

        private void OnEnable()
        {
            this.EventStartListening<OnLevelSelectedEvent>();
        }

        private void OnDisable()
        {
            this.EventStopListening<OnLevelSelectedEvent>();
        }

        /// <summary>
        /// Called when player select any level from levels screen
        /// </summary>
        /// <param name="eventType"></param>
        public void OnEvent(OnLevelSelectedEvent eventType)
        {
            SessionData.CurrentSelectedLevel = GameManager.Instance.GetLevel(eventType.levelId);
            OnSceneChangeRequestEvent.Trigger(Scenes.Gameplay,SessionData.touchPosition);
        }
    }
}