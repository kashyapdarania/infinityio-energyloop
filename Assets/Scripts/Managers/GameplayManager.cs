using DG.Tweening;
using EnergyLoop.EventsManager;
using EnergyLoop.Gameplay;
using TMPro;
using UnityEngine;

namespace EnergyLoop.Managers
{
    /// <summary>
    /// Responsible for handling all the managers
    /// </summary>
    public class GameplayManager : MonoBehaviour, EventListener<OnLevelCompletedEvent>
    {
        [SerializeField]
        private GridCreator gridCreator = default;
        [SerializeField]
        private NodeManager nodeManager = default;
        /// <summary>
        /// credit panel reference (shows when all level completes)
        /// </summary>
        [SerializeField]
        private Transform creditPanel = default;
        /// <summary>
        /// current level id ui text
        /// </summary>
        [SerializeField]
        private TMP_Text currentLevelText = default;

        private void Start()
        {
            LoadLevel();
        }

        /// <summary>
        /// Responsible for loading level
        /// </summary>
        private void LoadLevel()
        {
            // if this is last level, show credit screen otherwise load next level
            if (SessionData.CurrentSelectedLevel == null)
            {
                Utilities.CreateAppearSequence(creditPanel).Play();
                return;
            }

            // while we creating the level, we can set state to PreGameplay 
            GameManager.Instance.ChangeState(GameState.PreGameplay);

            LevelDetails currentLevel = SessionData.CurrentSelectedLevel;

            // update ui
            currentLevelText.text = "Level " + currentLevel.id;

            // create grid
            gridCreator.Create(currentLevel.size.x, currentLevel.size.y);

            // create nodes
            nodeManager.Create(gridCreator, currentLevel);

            // now all nodes are created, so changed state to gameplay 
            GameManager.Instance.ChangeState(GameState.Gameplay);
        }

        private void OnEnable()
        {
            this.EventStartListening<OnLevelCompletedEvent>();
        }

        private void OnDisable()
        {
            this.EventStopListening<OnLevelCompletedEvent>();
        }

        public void OnEvent(OnLevelCompletedEvent eventType)
        {
            // whenever level complete try to play next level
            this.WaitAndCall(1f, () =>
            {
                // increasing next level to play
                SessionData.CurrentSelectedLevel = GameManager.Instance.GetLevel(eventType.levelId + 1);

                // if this is last level, show credit screen otherwise load next level
                if (SessionData.CurrentSelectedLevel == null)
                {
                    Utilities.CreateAppearSequence(creditPanel).Play();
                    return;
                }
                OnSceneChangeRequestEvent.Trigger(Scenes.Gameplay, SessionData.touchPosition);
            });
        }

        /// <summary>
        /// Called when "Home" button click on Credit panel
        /// </summary>
        public void OnHomeButtonClicked()
        {
            // go back to menu screen
            SessionData.touchPosition = Input.mousePosition;
            OnSceneChangeRequestEvent.Trigger(Scenes.Menu, SessionData.touchPosition);
        }
    }
}
