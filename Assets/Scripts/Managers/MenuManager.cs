using DG.Tweening;
using EnergyLoop.EventsManager;
using UnityEngine;

namespace EnergyLoop.Managers
{
    /// <summary>
    /// Responsible for handle events of Menu screen
    /// </summary>
    public class MenuManager : MonoBehaviour, EventListener<OnGameStateChangeEvent>
    {
        /// <summary>
        /// Holding reference of Quit panel so we can display it to player
        /// </summary>
        [SerializeField]
        private Transform quitPanel = default;

        /// <summary>
        /// Holding reference of level panel so we can display it to player
        /// </summary>
        [SerializeField]
        private Transform levelPanel = default;

        private void OnEnable()
        {
            this.EventStartListening<OnGameStateChangeEvent>();
        }

        private void OnDisable()
        {
            this.EventStopListening<OnGameStateChangeEvent>();
        }

        /// <summary>
        /// Ccalled when "Play" button pressed
        /// </summary>
        public void OnPlayButtonClicked()
        {
            // start gameplay scene
            OnLevelSelectedEvent.Trigger(SessionData.LastCompletedLevel + 1);
        }

        /// <summary>
        /// Called when "Levels" button pressed
        /// </summary>
        public void OnLevelsButtonClicked()
        {
            // display levels
            OnGameStateChangeEvent.Trigger(SessionData.currentGameState, GameState.LevelScreen);
            Utilities.CreateAppearSequence(levelPanel).Play();
        }

        /// <summary>
        /// Called when "Back" button pressed on level screen
        /// </summary>
        public void OnBackButtonPressedOnLevelScreen()
        {
            OnGameStateChangeEvent.Trigger(SessionData.currentGameState, GameState.MenuScreen);
            Utilities.CreateDisappearSequence(levelPanel).Play();
        }

        /// <summary>
        /// Called when "Quit" button pressed
        /// </summary>
        public void OnQuitButtonClicked()
        {
            OnGameStateChangeEvent.Trigger(SessionData.currentGameState, GameState.Quit);
            Utilities.CreateAppearSequence(quitPanel).Play();
        }

        /// <summary>
        /// Called when "Yes" button pressed on QuitPanel
        /// </summary>
        public void OnPositiveButtonClicked()
        {
            Application.Quit();
        }

        /// <summary>
        /// Called when "No" button pressed on QuitPanel
        /// </summary>
        public void OnNegativeButtonClicked()
        {
            OnGameStateChangeEvent.Trigger(SessionData.currentGameState, GameState.MenuScreen);
            Utilities.CreateDisappearSequence(quitPanel).Play();
        }

        /// <summary>
        /// Called when game state changes
        /// </summary>
        /// <param name="eventType"></param>
        public void OnEvent(OnGameStateChangeEvent eventType)
        {
            switch (eventType.currentState)
            {
                case GameState.MenuScreen:
                    switch (eventType.previousState)
                    {
                        case GameState.LevelScreen:
                            Utilities.CreateDisappearSequence(levelPanel).Play();
                            break;
                        case GameState.Quit:
                            Utilities.CreateDisappearSequence(quitPanel).Play();
                            break;
                    }
                    break;
                case GameState.LevelScreen:
                    break;
                case GameState.Quit:
                    switch (eventType.previousState)
                    {
                        case GameState.MenuScreen:
                            Utilities.CreateAppearSequence(quitPanel).Play();
                            break;
                    }
                    break;
            }
        }
    }
}