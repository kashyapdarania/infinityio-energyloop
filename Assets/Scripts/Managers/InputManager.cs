using UnityEngine;

namespace EnergyLoop.Managers
{
    /// <summary>
    /// Responsible for adding draggable functionality to objects
    /// </summary>
    public interface IDraggable
    {
        bool OnDragStart();
        void OnDragging();
        void OnDragEnd();
        void OnReset();

        Transform transform { get; }
    }

    /// <summary>
    /// Responsible for handling inputs
    /// </summary>
    public class InputManager : MonoBehaviour
    {
        // raycast will ignore other layers than this mask(can also set using << operator)
        public LayerMask nodeMask;

        [SerializeField]
        private NodeManager nodeManage = default;

        private IDraggable currentDraggingNode;
        /// <summary>
        /// world position of Input.mousepotion
        /// </summary>
        Vector3 worldPosition;
        /// <summary>
        /// difference between raycast hit and the own position so while dragging, it drag from where we start dragging 
        /// instead of transform.position
        /// </summary>
        Vector3 offset;

        private void Start()
        {
            // disabling multiTouch 
            Input.multiTouchEnabled = false;
        }

        private void Update()
        {
            // NOTE: currently I am using mouse event. Because this can also works in editor. It is fine until we are not going
            // for multi-finger. If want to use multi-finger, we can use Input.GetTouch.

            // if currenlty state is not gameplay then we are not intersted in any inputs
            if (SessionData.currentGameState != GameState.Gameplay)
            {
                return;
            }

            // mouse down or touch down
            if (Input.GetMouseButtonDown(0))
            {
                // converting mouse position to world position for raycast
                worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                // raycasting (ignoring other layers except "Node")
                var hit = Physics2D.Raycast(worldPosition, Vector2.zero, 0f, nodeMask);
                // if something hit
                if (hit)
                {
                    // if draggable is null on hitted object, no need to do anything
                    currentDraggingNode = hit.transform.GetComponent<IDraggable>();
                    if (currentDraggingNode != null)
                    {
                        // calculating ofset
                        offset = hit.point;
                        offset = hit.transform.position - offset;

                        // if draggable is fine with dragging then it will start dragging,
                        // if draggable don't want to drag for specific reason, it has to return false.
                        if (!currentDraggingNode.OnDragStart())
                        {
                            currentDraggingNode = null;
                        }
                    }
                }
            }
            // mouse up or touch up
            else if (Input.GetMouseButtonUp(0))
            {
                if (currentDraggingNode != null)
                {
                    worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    // raycating all because now we have to find any other node at current mouse position,
                    // if we find any node, then we have to swith if it is not static
                    var nodes = Physics2D.RaycastAll(worldPosition, Vector2.zero);

                    // if there are no nodes at mouse position, then we just have to reset current dragging object
                    if (nodes == null || nodes.Length <= 1)
                    {
                        currentDraggingNode.OnDragEnd();
                        currentDraggingNode.OnReset();
                        currentDraggingNode = null;
                        return;
                    }

                    // finding existing node on the grid and swap them
                    var existingNode = GetExistingNode(nodes);
                    if (existingNode != null)
                    {
                        SessionData.touchPosition = Input.mousePosition;
                        nodeManage.SwitchNode(currentDraggingNode, existingNode);
                    }

                    currentDraggingNode = null;
                }
            }

            // mouse dragging or touch dragging
            else if (Input.GetMouseButton(0))
            {
                if (currentDraggingNode != null)
                {
                    // convert mouse position to world position
                    worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    // add offset to world position
                    worldPosition = worldPosition + offset;
                    // reset z position as we are working in 2D
                    worldPosition.z = 0;
                    // update current dragging node position
                    currentDraggingNode.transform.position = worldPosition;

                    currentDraggingNode.OnDragging();
                }
            }
        }

        /// <summary>
        /// will return node that already on the grid except current dragging node when mouse up (first will return if there
        /// are many nodes available)
        /// </summary>
        /// <param name="nodes">nodes to check</param>
        /// <returns></returns>
        private IDraggable GetExistingNode(RaycastHit2D[] nodes)
        {
            for (int i = 0; i < nodes.Length; i++)
            {
                if (nodes[i].transform != currentDraggingNode.transform)
                {
                    return nodes[i].transform.GetComponent<IDraggable>();
                }
            }

            return null;
        }
    }
}
