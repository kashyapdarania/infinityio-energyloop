using EnergyLoop.EventsManager;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EnergyLoop.Managers
{
    public enum Scenes
    {
        Menu = 0,
        Gameplay = 1
    }

    /// <summary>
    /// Responsible for scene management
    /// </summary>
    public class SceneManager : MonoBehaviour, EventListener<OnSceneChangeRequestEvent>
    {
        /// <summary>
        /// Radius of the fill effect (should be same as shader)
        /// </summary>
        private const float MAXIMUM_RADIUS = 15;

        /// <summary>
        /// how fast the fill effect should complete?
        /// </summary>
        private const float TRANSITION_EFFECT_SPEED = 2;

        /// <summary>
        /// Wait time before showing content behind fill effect
        /// </summary>
        private const float WAIT_TIME = 0.3f;

        private int RADIUS_PROPERTY_ID = Shader.PropertyToID("_Radius");
        private int CENTER_PROPERTY_ID = Shader.PropertyToID("_Center");

        public static SceneManager Instance;

        /// <summary>
        /// Material used to show fill effect
        /// </summary>
        [SerializeField]
        private Material transitionMaterial = default;

        /// <summary>
        /// Responsible when fill effect is on-going then other elements should not be interactable
        /// </summary>
        [SerializeField]
        private GameObject eventBlocker = default;

        private bool displayTransitionEffect = false;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
                return;
            }

            // reset material property
            transitionMaterial.SetFloat(RADIUS_PROPERTY_ID, MAXIMUM_RADIUS);
            transitionMaterial.SetVector(CENTER_PROPERTY_ID, Vector3.zero);
            eventBlocker.SetActive(false);
        }

        private void OnEnable()
        {
            this.EventStartListening<OnSceneChangeRequestEvent>();
            UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneLoaded;
        }

        private void OnDisable()
        {
            this.EventStopListening<OnSceneChangeRequestEvent>();
            UnityEngine.SceneManagement.SceneManager.sceneLoaded -= SceneLoaded;
        }

        /// <summary>
        /// Called by Unity when scene's loading complete
        /// </summary>
        /// <param name="scene">loaded scene</param>
        /// <param name="mode">mode</param>
        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (!displayTransitionEffect)
            {
                // Transition completed
                eventBlocker.SetActive(false);
                return;
            }

            StopAllCoroutines();

            // once scene is loaded, display transition
            StartCoroutine(TransitionEffect(0, MAXIMUM_RADIUS, () =>
            {
                // Transition completed
                transitionMaterial.SetVector(CENTER_PROPERTY_ID, Vector3.zero);
                displayTransitionEffect = false;
                eventBlocker.SetActive(false);
            }, WAIT_TIME));
        }

        public void OnEvent(OnSceneChangeRequestEvent eventType)
        {
            eventBlocker.SetActive(true);
            displayTransitionEffect = true;

            // update center
            Vector3 center;

            if (!eventType.touchPosition.Equals(Vector2.zero))
            {
                center = Camera.main.ScreenToWorldPoint(eventType.touchPosition);
                center.z = 0;
            }
            else
            {
                center = Vector3.zero;
            }
            transitionMaterial.SetVector(CENTER_PROPERTY_ID, center);

            // transition effect
            StartCoroutine(TransitionEffect(MAXIMUM_RADIUS, 0, () =>
            {
                // Transition completed
                UnityEngine.SceneManagement.SceneManager.LoadScene((int)eventType.sceneId);
            }));
        }

        /// <summary>
        /// Transition effect coroutine
        /// </summary>
        /// <param name="initialValue">fill initial value</param>
        /// <param name="finalValue">fill final value</param>
        /// <param name="callback">callback when fill effect complete</param>
        /// <param name="initialWait">wait before fill effect starts</param>
        /// <returns></returns>
        private IEnumerator TransitionEffect(float initialValue, float finalValue, Action callback = null, float initialWait = 0)
        {
            // wait for specific time
            yield return new WaitForSeconds(initialWait);

            var delay = new WaitForEndOfFrame();
            float timer = 0;
            do
            {
                transitionMaterial.SetFloat(RADIUS_PROPERTY_ID, Mathf.Lerp(initialValue, finalValue, timer));
                timer += Time.deltaTime * TRANSITION_EFFECT_SPEED;
                yield return delay;
            } while (timer <= 1f);

            // seting to final value 
            transitionMaterial.SetFloat(RADIUS_PROPERTY_ID, finalValue);

            callback?.Invoke();
        }
    }
}
