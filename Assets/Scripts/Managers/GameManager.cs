using EnergyLoop.EventsManager;
using System.Linq;
using UnityEngine;

namespace EnergyLoop.Managers
{
    /// <summary>
    /// states of the game at a perticular time
    /// </summary>
    public enum GameState
    {
        MenuScreen,
        LevelScreen,
        Quit,
        PreGameplay,
        Gameplay,
        PostGameplay,
        LevelComplete
    }

    /// <summary>
    /// Responsible for handling whole game(flow)
    /// </summary>
    public class GameManager : MonoBehaviour,
                               EventListener<OnGameStateChangeEvent>,
                               EventListener<OnLevelCompletedEvent>
    {
        public static GameManager Instance;

        [SerializeField]
        private Levels levels = default;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        private void Start()
        {
            SessionData.currentGameState = GameState.MenuScreen;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        private void OnEnable()
        {
            this.EventStartListening<OnGameStateChangeEvent>();
            this.EventStartListening<OnLevelCompletedEvent>();
        }

        private void OnDisable()
        {
            this.EventStopListening<OnGameStateChangeEvent>();
            this.EventStopListening<OnLevelCompletedEvent>();
        }

        /// <summary>
        /// Get currently playing level details
        /// </summary>
        /// <returns>level's detail</returns>
        public Levels GetLevels()
        {
            return levels;
        }

        /// <summary>
        /// Get level details by level Id
        /// </summary>
        /// <param name="levelId">level id</param>
        /// <returns>level's detail</returns>
        public LevelDetails GetLevel(int levelId)
        {
            return levels.levels.SingleOrDefault(level => level.id == levelId);
        }

        /// <summary>
        /// Change state
        /// </summary>
        /// <param name="state">target state</param>
        public void ChangeState(GameState state)
        {
            // no need to update state if currentstate is same
            if (SessionData.currentGameState == state)
            {
                return;
            }

            OnGameStateChangeEvent.Trigger(SessionData.currentGameState, state);
            SessionData.currentGameState = state;
        }

        private void Update()
        {
            // handing back button of the mobile devices and pc
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                switch (SessionData.currentGameState)
                {
                    case GameState.MenuScreen:
                        ChangeState(GameState.Quit);
                        break;
                    case GameState.LevelScreen:
                        ChangeState(GameState.MenuScreen);
                        break;
                    case GameState.Quit:
                        ChangeState(GameState.MenuScreen);
                        break;
                    case GameState.PreGameplay:
                    case GameState.Gameplay:
                    case GameState.PostGameplay:
                    case GameState.LevelComplete:
                        ChangeState(GameState.MenuScreen);
                        OnSceneChangeRequestEvent.Trigger(Scenes.Menu, Vector2.zero);
                        break;
                }
            }
        }

        /// <summary>
        /// Called when game state changed
        /// </summary>
        /// <param name="eventType"></param>
        public void OnEvent(OnGameStateChangeEvent eventType)
        {
            if (eventType.currentState == eventType.previousState)
            {
                return;
            }

            SessionData.currentGameState = eventType.currentState;
        }

        /// <summary>
        /// Called on level complete
        /// </summary>
        /// <param name="eventType"></param>
        public void OnEvent(OnLevelCompletedEvent eventType)
        {
            SessionData.currentGameState = GameState.LevelComplete;
        }
    }
}