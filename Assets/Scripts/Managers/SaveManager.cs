using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

namespace EnergyLoop.Managers
{
    [Serializable]
    public class NodeSaveData
    {
        public List<NodeData> nodes;
    }


    // Generally we can save the progress to the files when some event occurs, like player clicks on the save button or back button
    // but in such situations, if player make some progress and kill the application then if we write our code to save data in Disable or Destroy
    // then there is a possibility that our code might not executes becuase the main process already killed by player.
    // So I am saving data to file whenever there are swap possible between two nodes
    // We can use this here becuase we don't have to much of data to write but whenever we have lots of data then we need to try to 
    // normalized the data (same as database normalization) so we can reduce the write operation
    // here also I am using Threads to save data thus providing smooth experience to player 

    /// <summary>
    /// Responsible for saving and loading content from the file
    /// </summary>
    public class SaveManager : MonoBehaviour
    {
        public static SaveManager Instance;

        NodeSaveData nodesToSave;
        int currentLevel;
        int levelCompleted;

        Thread threadForSavingNodes;
        Thread threadForSavingCompletedLevel;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        private void Start()
        {
            print(Application.persistentDataPath);

            // create levels directory in the specific path so we can save levels in future.
            DirectoryInfo levelsDirectory = new DirectoryInfo(Utilities.LEVELS_SAVE_PATH);
            if (!levelsDirectory.Exists)
            {
                levelsDirectory.Create();
            }
            else
            {
                // try to delete level files those are not necessary
                // This is to make sure that all completed levels file is not there
                DeleteCompletedLevelsIfAny(levelsDirectory);
            }
        }

        /// <summary>
        /// try to delete level files those are not necessary
        //  This is to make sure that all completed levels file is not there
        /// </summary>
        /// <param name="levelDirectory"></param>
        private void DeleteCompletedLevelsIfAny(DirectoryInfo levelDirectory)
        {
            if (levelDirectory == null)
            {
                return;
            }
            FileInfo[] files = levelDirectory.GetFiles();

            // no need to delete files if there is not any
            if (files.Length <= 0)
            {
                return;
            }

            int lastCompletedLevel = GetLastCompletedLevel();
            string fileName;
            int level;
            for (int i = 0; i < files.Length; i++)
            {
                fileName = files[i].Name;
                if (string.IsNullOrEmpty(fileName))
                {
                    continue;
                }

                level = Convert.ToInt32(Path.GetFileNameWithoutExtension(fileName));
                if (level <= lastCompletedLevel)
                {
                    files[i].Delete();
                }
            }
        }

        /// <summary>
        /// Save nodes data to the file (Thread)
        /// </summary>
        private void SaveProgress()
        {
            string path = Path.Combine(Utilities.LEVELS_SAVE_PATH, currentLevel + ".json");
            string json = JsonUtility.ToJson(nodesToSave);
            File.WriteAllText(path, json);
        }

        /// <summary>
        /// Save last completed level to the file
        /// </summary>
        private void SaveCompletedLevel()
        {
            try
            {
                string path = Path.Combine(Utilities.LAST_COMPLETED_SAVE_PATH);
                File.WriteAllText(path, levelCompleted.ToString());

                // Delete json for completed level so next then we play existing level, all nodes will arrange in random order
                string levelPath = Path.Combine(Utilities.LEVELS_SAVE_PATH, levelCompleted + ".json");
                File.Delete(levelPath);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        /// <summary>
        /// Return nodes data for specific level from the file
        /// </summary>
        /// <param name="currentLevel">level to load</param>
        /// <returns></returns>
        public NodeSaveData LoadLevel(int currentLevel)
        {
            string path = Path.Combine(Utilities.LEVELS_SAVE_PATH, currentLevel + ".json");
            if (!File.Exists(path))
            {
                return null;
            }
            string json = File.ReadAllText(path);
            return JsonUtility.FromJson<NodeSaveData>(json);
        }

        /// <summary>
        /// Save nodes data to the file (public method)
        /// </summary>
        /// <param name="currentLevel">current level</param>
        /// <param name="nodesToSave">nodes to save</param>
        public void SaveLevel(int currentLevel, NodeSaveData nodesToSave)
        {
            this.nodesToSave = nodesToSave;
            this.currentLevel = currentLevel;

            // starting a thread so there is no lag in the gameplay experience
            threadForSavingNodes = new Thread(SaveProgress);
            threadForSavingNodes.Start();
        }

        /// <summary>
        /// Return last completed level
        /// </summary>
        /// <returns></returns>
        public int GetLastCompletedLevel()
        {
            string path = Path.Combine(Utilities.LAST_COMPLETED_SAVE_PATH);
            try
            {
                return Convert.ToInt32(File.ReadAllText(path));
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Called when this object destroy
        /// </summary>
        private void OnDestroy()
        {
            // if there is any thread running, abort that thread
            if (threadForSavingNodes != null)
            {
                if (threadForSavingNodes.IsAlive)
                {
                    threadForSavingNodes.Abort();
                }
            }

            if (threadForSavingCompletedLevel != null)
            {
                if (threadForSavingCompletedLevel.IsAlive)
                {
                    threadForSavingCompletedLevel.Abort();
                }
            }
        }

        /// <summary>
        /// Save last completed level to the file (public method)
        /// </summary>
        /// <param name="levelCompleted"></param>
        public void SaveLastCompletedLevel(int levelCompleted)
        {
            this.levelCompleted = levelCompleted;
            threadForSavingCompletedLevel = new Thread(SaveCompletedLevel);
            threadForSavingCompletedLevel.Start();
        }
    }
}
