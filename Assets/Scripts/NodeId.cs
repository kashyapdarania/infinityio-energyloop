using UnityEngine;

namespace EnergyLoop
{
    /// <summary>
    /// Used for only Unique identification
    /// </summary>
    public class NodeId : MonoBehaviour
    {
        public int id;
    }
}
