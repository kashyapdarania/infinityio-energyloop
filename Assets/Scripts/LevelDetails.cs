using System;
using System.Collections.Generic;
using UnityEngine;

namespace EnergyLoop
{

    /// <summary>
    /// Node details
    /// </summary>
    [Serializable]
    public class NodeData
    {

        /// <summary>
        /// Node id (unique)
        /// </summary>
        [HideInInspector]
        public int id;

        /// <summary>
        /// which nodes should be used
        /// </summary>
        [Tooltip("Which nodes should be use at this position from Resources/Nodes")]
        public int nodeId;

        /// <summary>
        /// position of the node in grid (x = row, y = column)
        /// </summary>
        [Tooltip("Position in the grid (x = row, y = column)")]
        public Vector2Int position;
    }

    /// <summary>
    /// Scriptable object for storing Level Details
    /// </summary>
    [CreateAssetMenu(fileName = "Level_N")]
    public class LevelDetails : ScriptableObject
    {
        /// <summary>
        /// level id
        /// </summary>
        public int id;

        /// <summary>
        /// Size of grid (x = rows, y = columns)(We can also use another struct with row and column field instead of using Vector2Int)
        /// </summary>
        /// 
        [Tooltip("Size of the grid (x = rows, y = columns)")]
        public Vector2Int size;

        /// <summary>
        /// List of static nodes(nodes those player can't control)
        /// </summary>
        public List<NodeData> staticNodes;

        /// <summary>
        /// List of dynamic nodes(nodes player can control)
        /// </summary>
        public List<NodeData> dynamicNodes;
    }
}