using System.Collections.Generic;
using UnityEngine;

namespace EnergyLoop.Gameplay
{
    /// <summary>
    /// Responsible for create grid
    /// </summary>
    public class GridCreator : MonoBehaviour
    {
        private Vector2 gridSize = default;

        [SerializeField]
        private Canvas canvas;

        /// <summary>
        /// Where all cells should be create?
        /// </summary>
        [SerializeField]
        private Transform gridParent = default;

        /// <summary>
        /// Cell prefab
        /// </summary>
        [SerializeField]
        private Cell cellPrefab = default;

        private List<Vector3> cellsPosition;

        private int totalRows;
        private int totalColumns;

        private void Awake()
        {
            cellsPosition = new List<Vector3>();
        }

        /// <summary>
        /// Create grid with specific size
        /// </summary>
        /// <param name="rows">rows</param>
        /// <param name="columns">columns</param>
        public void Create(int rows, int columns)
        {
            totalRows = rows;
            totalColumns = columns;
            InitCells(rows, columns);
        }

        /// <summary>
        /// Create grid with specific size
        /// </summary>
        /// <param name="size">rows and columns</param>
        public void Create(Vector2Int size)
        {
            totalRows = size.x;
            totalColumns = size.y;
            InitCells(size.x, size.y);
        }

        /// <summary>
        /// Init cell creation
        /// </summary>
        /// <param name="rows">rows</param>
        /// <param name="cols">columns</param>
        private void InitCells(int rows, int cols)
        {
            gridSize = new Vector2(cols, rows);

            CreateCells(rows, cols);

            // handle multi-resolution
            MultiResolution.SetupWorld(transform, rows, cols, gridSize);

            CreateBackground();
        }

        /// <summary>
        /// Creating grid background
        /// </summary>
        private void CreateBackground()
        {
            Cell background = Instantiate(cellPrefab, this.transform);
            background.name = "GridBackground";
            background.GetComponent<SpriteRenderer>().enabled = true;

            // scale to fit the size of the grid
            Vector2 scale = new Vector2(background.transform.localScale.x, background.transform.localScale.y);
            scale.x *= totalColumns;
            scale.y *= totalRows;

            // to display outer border, increase size as per node scale
            scale.x += 1 - Node.INITIAL_SCALE_MULTIPLIER;
            scale.y += 1 - Node.INITIAL_SCALE_MULTIPLIER;

            background.transform.localScale = scale;
        }

        /// <summary>
        /// Creating cells (Cells is only for position purpose currently. It can be used to extend the game)
        /// </summary>
        /// <param name="rows">rows</param>
        /// <param name="cols">columns</param>
        void CreateCells(int rows, int cols)
        {
            Cell cellObject = Instantiate(cellPrefab);

            Vector2 cellSize;

            cellSize = cellObject.GetComponent<SpriteRenderer>().bounds.size;

            // cell size of the grid
            Vector2 newCellSize = new Vector2(gridSize.x / (float)cols, gridSize.y / (float)rows);

            Vector2 cellScale;
            cellScale.x = newCellSize.x / cellSize.x;
            cellScale.y = newCellSize.y / cellSize.y;

            cellSize = newCellSize;

            cellObject.transform.localScale = new Vector2(cellScale.x, cellScale.y);
            cellSize = new Vector2(cellScale.x, cellScale.y);

            // to keep grid at center
            Vector2 gridOffset;
            gridOffset.x = -(gridSize.x / 2) + cellSize.x / 2;
            gridOffset.y = -(gridSize.y / 2) + cellSize.y / 2;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    // finding new position for current cell as per row and column
                    Vector3 pos = new Vector3(col * cellSize.x + gridOffset.x + transform.position.x, row * cellSize.y + gridOffset.y + transform.position.y, 0);
                    // create new cell
                    Cell cell = Instantiate(cellObject, pos, Quaternion.identity);
                    cell.transform.parent = gridParent;
                    cellsPosition.Add(cell.transform.localPosition);
                }
            }

            // destroy temporary cell
            Destroy(cellObject.gameObject);
        }

        /// <summary>
        /// Cell position by row and column
        /// </summary>
        /// <param name="row">row</param>
        /// <param name="column">column</param>
        /// <returns>position</returns>
        public Vector3 GetCellPosition(int row, int column)
        {
            int index = row * totalColumns + column;
            Vector3 position = cellsPosition[index];
            position.z = 0;
            return position * transform.localScale.x;
        }

        //so you can see the width and height of the grid on editor
        void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, gridSize);
        }
    }
}