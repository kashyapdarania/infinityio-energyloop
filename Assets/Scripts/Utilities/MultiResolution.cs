using UnityEngine;

namespace EnergyLoop
{
    /// <summary>
    /// Handles multi-resolution
    /// </summary>
    public class MultiResolution
    {
        private const int TOP_PADDING = 200;
        private const int SIDE_PADDING = 30;
        private const float CAMERA_ZOOM_AMOUNT = 1.8f;

        public static void SetupWorld(Transform transform, int rows, int cols, Vector3 gridSize)
        {
            // fit in height (keep gap so there is visible area surrounding grid)
            float l_CameraYPos = Screen.height / (CAMERA_ZOOM_AMOUNT * 100);
            Camera.main.orthographicSize = l_CameraYPos;

            float currentRatio = Screen.height / (float)Screen.width;
            float gridRatio = rows / (float)cols;

            if (gridRatio > currentRatio)
            {
                float l_ScreenHeight = Screen.height - (TOP_PADDING * (Screen.height / (float)Utilities.REFERENCE_HEIGHT));
                transform.localScale *= (l_ScreenHeight / (gridSize.y * Utilities.PIXEL_PER_UNIT));
            }
            else
            {
                float l_ScreenWidth = Screen.width - (SIDE_PADDING * (Screen.width / (float)Utilities.REFERENCE_WIDTH));
                transform.localScale *= (l_ScreenWidth / (gridSize.x * Utilities.PIXEL_PER_UNIT));
            }
        }
    }
}