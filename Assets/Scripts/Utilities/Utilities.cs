using DG.Tweening;
using System;
using System.Collections;
using System.IO;
using UnityEngine;

namespace EnergyLoop
{
    /// <summary>
    /// Hold utilities functions and fields
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Sets of color for the non-static nodes
        /// </summary>
        private static Color[] nodeColors =
            {
            new Color(0, 0.7372549f, 0.4862745f),
            new Color(1, 0.1411765f, 0.2588235f),
            new Color(1, 0.1411765f, 0.2588235f),
            new Color(0, 0.7960784f, 0.945098f)
        };

        /// <summary>
        /// Static node color
        /// </summary>
        public static Color staticNodeColor = new Color(0.1294118f, 0.1294118f, 0.1294118f);

        /// <summary>
        /// Reference resolution width
        /// </summary>
        public const int REFERENCE_WIDTH = 1080;
        /// <summary>
        /// Reference resolution height
        /// </summary>
        public const int REFERENCE_HEIGHT = 1920;

        /// <summary>
        /// Levels path (where to save and load)
        /// </summary>
        public static string LEVELS_SAVE_PATH = Path.Combine(Application.persistentDataPath, "Levels");
        /// <summary>
        /// Completed levels path (where to save and load)
        /// </summary>
        public static string LAST_COMPLETED_SAVE_PATH = Path.Combine(Application.persistentDataPath, "LastCompletedLevel.json");

        /// <summary>
        /// Pixels per unit to calculate multi resolution
        /// </summary>
        public const float PIXEL_PER_UNIT = 100;

        /// <summary>
        /// Scale when appearing
        /// </summary>
        private const float AppearanceScale = 1.1f;

        /// <summary>
        /// time for appearing animation
        /// </summary>
        private const float AppearanceTime = 0.1f;

        /// <summary>
        /// time for appearing reset animation (0 to 1.1 and 1.1 to 1) (1.1 to 1 is reset animation)
        /// </summary>
        private const float AppearanceResetTime = 0.1f;

        /// <summary>
        /// return random node color from sets of colors.
        /// </summary>
        /// <returns></returns>
        public static Color GetNodeColor()
        {
            return nodeColors[UnityEngine.Random.Range(0, nodeColors.Length)];
        }

        /// <summary>
        /// Used to create sequence for appear
        /// </summary>
        /// <param name="target">target</param>
        /// <returns>sequence</returns>
        public static Sequence CreateAppearSequence(Transform target)
        {
            var sequenceToAppear = DOTween.Sequence();
            sequenceToAppear.Append(target.DOScale(AppearanceScale, AppearanceTime));
            sequenceToAppear.Append(target.DOScale(1f, AppearanceResetTime));
            sequenceToAppear.SetEase(Ease.InCirc);
            return sequenceToAppear;
        }

        /// <summary>
        /// Used to create sequence for disappear
        /// </summary>
        /// <param name="target">target</param>
        /// <returns>sequence</returns>
        public static Sequence CreateDisappearSequence(Transform target)
        {
            var sequenceToDisappear = DOTween.Sequence();
            sequenceToDisappear.Append(target.DOScale(0f, AppearanceResetTime));
            sequenceToDisappear.SetEase(Ease.InCirc);
            return sequenceToDisappear;
        }

        /// <summary>
        /// Wait and call the action
        /// </summary>
        /// <param name="behaviour">on monobehaviour, you can use this method.</param>
        /// <param name="delay">wait in the seconds</param>
        /// <param name="callback">after time complete, callback to call</param>
        public static void WaitAndCall(this MonoBehaviour behaviour, float delay, Action callback)
        {
            behaviour.StartCoroutine(WaitAndCallCoroutine(delay, callback));
        }

        private static IEnumerator WaitAndCallCoroutine(float delay, Action callback)
        {
            yield return new WaitForSeconds(delay);
            callback?.Invoke();
        }
    }
}
