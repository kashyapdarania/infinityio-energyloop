using EnergyLoop.Managers;
using UnityEngine;
namespace EnergyLoop
{
    /// <summary>
    /// Hold data so we can use in another scenes
    /// </summary>
    public static class SessionData
    {
        /// <summary>
        /// current selected level's details
        /// </summary>
        public static LevelDetails CurrentSelectedLevel;

        /// <summary>
        /// current state of the game
        /// </summary>
        public static GameState currentGameState;

        /// <summary>
        /// Last completed level
        /// </summary>
        public static int LastCompletedLevel = 0;

        /// <summary>
        /// Last touch position
        /// </summary>
        public static Vector3 touchPosition;
    }
}