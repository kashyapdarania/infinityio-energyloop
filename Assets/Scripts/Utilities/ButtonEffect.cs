using DG.Tweening;
using EnergyLoop.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace EnergyLoop.UI.Effects
{
    /// <summary>
    /// Responsible for adding appear and click effects
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class ButtonEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        // I used to name const variable as FIRST_TIME_APPEARANCE_SCALE
        private const float PressedStateScale = 0.9f;
        private const float PointerEventResetTime = 0.1f;

        private Button btn;

        private void Awake()
        {
            transform.localScale = Vector3.zero;
            btn = GetComponent<Button>();
        }

        private void OnEnable()
        {
            // Display appear animation
            Utilities.CreateAppearSequence(transform).Play();
        }

        /// <summary>
        /// Will be called when player tap down this button
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            if (!btn.interactable)
            {
                return;
            }

            SoundManager.Instance.PlaySFX(SFXType.ButtonTap);

            // animating it to creating click effect
            transform.DOScale(PressedStateScale, PointerEventResetTime);
        }

        /// <summary>
        /// Will be called when player tap up this button
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerUp(PointerEventData eventData)
        {
            if (!btn.interactable)
            {
                return;
            }
            // reset to normal state
            transform.DOScale(1f, PointerEventResetTime);
        }
    }
}
