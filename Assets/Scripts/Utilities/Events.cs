
using EnergyLoop.EventsManager;
using EnergyLoop.Managers;
using UnityEngine;

namespace EnergyLoop
{
    public struct OnGameStateChangeEvent
    {
        public static OnGameStateChangeEvent current;

        public GameState currentState;
        public GameState previousState;
        public static void Trigger(GameState previousState, GameState currentState)
        {
            current.previousState = previousState;
            current.currentState = currentState;
            EventManager.TriggerEvent<OnGameStateChangeEvent>(current);
        }
    }

    public struct OnLevelCompletedEvent
    {
        public static OnLevelCompletedEvent current;

        public int levelId;
        public static void Trigger(int levelId)
        {
            current.levelId = levelId;
            EventManager.TriggerEvent<OnLevelCompletedEvent>(current);
        }
    }

    public struct OnLevelSelectedEvent
    {
        public static OnLevelSelectedEvent current;

        public int levelId;
        public static void Trigger(int levelId)
        {
            current.levelId = levelId;
            EventManager.TriggerEvent<OnLevelSelectedEvent>(current);
        }
    }

    public struct OnSceneChangeRequestEvent
    {
        public static OnSceneChangeRequestEvent current;

        public Scenes sceneId;
        public Vector2 touchPosition;

        public static void Trigger(Scenes sceneId, Vector2 touchPosition)
        {
            current.sceneId = sceneId;
            current.touchPosition = touchPosition;

            EventManager.TriggerEvent<OnSceneChangeRequestEvent>(current);
        }
    }
}
