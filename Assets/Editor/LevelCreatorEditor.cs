using EnergyLoop.Gameplay;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace EnergyLoop.Editor
{

    /// <summary>
    /// Responsible for providing utilities to designer to create new levels
    /// </summary>
    public class LevelCreatorEditor : EditorWindow
    {
        int levelId;
        Vector2Int gridSize;
        Levels levels;
        LevelDetails levelDetails;

        /// <summary>
        /// Responsible for showing window when selecting "Tools/Create Level" from menubar in the Unity
        /// </summary>
        [MenuItem("Tools/Create Level")]
        public static void ShowCreatorWindow()
        {
            LevelCreatorEditor window = (LevelCreatorEditor)EditorWindow.GetWindow(typeof(LevelCreatorEditor));
            window.titleContent = new GUIContent("Level Creator");
            window.Show();
        }

        private void OnEnable()
        {
            EditorApplication.hierarchyChanged += EditorApplicationHierarchyChanged;
        }

        private void OnDisable()
        {
            EditorApplication.hierarchyChanged -= EditorApplicationHierarchyChanged;
        }

        /// <summary>
        /// Will be called by UnityEngine when new object drops into the scene
        /// </summary>
        private void EditorApplicationHierarchyChanged()
        {
            var gameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            Node node;
            foreach (var gameObject in gameObjects)
            {
                node = gameObject.GetComponent<Node>();
                if (node != null)
                {
                    gameObject.transform.position = RoundVector(gameObject.transform.position);
                    node.positionInGrid = new Vector2Int((int)gameObject.transform.position.y, (int)gameObject.transform.position.x);
                }
            }
        }

        void OnGUI()
        {
            EditorGUILayout.BeginVertical();

            // Save level functionality
            SaveLevel();

            EditorGUILayout.Space(20);

            // Delete nodes from the current scene
            DeleteNodes();

            EditorGUILayout.Space(20);

            // Toggle selected node is static or not?
            ToggleStatic();

            EditorGUILayout.Space(30);

            // Assign newly created level to the list
            AssignLevelToList();

            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Save level functionality
        /// </summary>
        private void SaveLevel()
        {
            EditorGUILayout.LabelField("Create Level");

            levelId = EditorGUILayout.IntField("Level Number:", levelId);
            gridSize = EditorGUILayout.Vector2IntField("Grid Size:", gridSize);

            if (GUILayout.Button("Save"))
            {
                levelDetails = ScriptableObject.CreateInstance<LevelDetails>();

                levelDetails.id = levelId;
                levelDetails.size = gridSize;

                levelDetails.staticNodes = new List<NodeData>();
                levelDetails.dynamicNodes = new List<NodeData>();

                NodeId nodeId;
                Node node;
                var gameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
                // order objects using id

                var nodes = new List<Node>();
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    // rounding again in case someone moves object via Gizmos
                    node = gameObjects[i].GetComponent<Node>();
                    if (node != null)
                    {
                        node.transform.position = RoundVector(node.transform.position);
                        node.positionInGrid = new Vector2Int((int)node.transform.position.y, (int)node.transform.position.x);
                        nodes.Add(node);
                    }
                }

                nodes = nodes.OrderBy(n => n.GetComponent<NodeId>().id).ToList();

                foreach (var localNode in nodes)
                {
                    nodeId = localNode.GetComponent<NodeId>();
                    if (localNode.Static)
                    {
                        levelDetails.staticNodes.Add(new NodeData() { nodeId = nodeId.id, position = localNode.positionInGrid });
                    }
                    else
                    {
                        levelDetails.dynamicNodes.Add(new NodeData() { nodeId = nodeId.id, position = localNode.positionInGrid });
                    }
                }
                string path = "Assets/Levels/Level_" + levelId + ".asset";
                AssetDatabase.DeleteAsset(path);
                AssetDatabase.CreateAsset(levelDetails, path);
                AssetDatabase.SaveAssets();

                Debug.Log("Level saved at " + path);
            }
        }

        /// <summary>
        /// Delete nodes from the current scene
        /// </summary>
        private static void DeleteNodes()
        {
            if (GUILayout.Button("Delete Nodes from Scene"))
            {
                var gameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
                foreach (var gameObject in gameObjects)
                {
                    if (gameObject.GetComponent<Node>() != null)
                    {
                        DestroyImmediate(gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Toggle selected node is static or not?
        /// </summary>
        private void ToggleStatic()
        {
            if (GUILayout.Button("Toggle static on/off - selected Node"))
            {
                if (Selection.activeObject == null)
                {
                    Debug.LogWarning("Please select any node!");
                    return;
                }

                GameObject nodeSelection = Selection.activeObject as GameObject;
                Node node = nodeSelection.GetComponent<Node>();
                if (node == null)
                {
                    Debug.LogWarning("Please select any node!");
                    return;
                }

                node.Static = !node.Static;
                Debug.Log("Static set to : " + node.Static);
            }
        }

        /// <summary>
        /// Assign newly created level to the list
        /// </summary>
        private void AssignLevelToList()
        {
            levels = (Levels)EditorGUILayout.ObjectField("Levels List:", levels, typeof(Levels), false);

            EditorGUILayout.HelpBox("Plase save level before adding to the list", MessageType.Info);
            if (GUILayout.Button("Assign current level to Levels List"))
            {
                if (levels == null)
                {
                    Debug.LogError("Please assign LevelsList");
                    return;
                }

                if (levelDetails != null)
                {
                    if (!levels.levels.Contains(levelDetails))
                    {
                        levels.levels.Add(levelDetails);
                        Debug.Log(levelDetails.id + " added to list");
                    }
                }
            }
        }

        /// <summary>
        /// Round vector's components
        /// </summary>
        /// <param name="vector">vector to round</param>
        /// <returns>rounded vector</returns>
        private Vector3 RoundVector(Vector3 vector)
        {
            vector.x = (int)Math.Round(vector.x, 0);
            vector.y = (int)Math.Round(vector.y, 0);
            vector.z = (int)Math.Round(vector.z, 0);
            return vector;
        }
    }
}
